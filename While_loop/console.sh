#!/bin/bash

while read inp
do
  if [[ $inp == "pwd" ]]
    then
    pwd
  elif [[ $inp == "ls ~" ]]
    then    
    ls ~
  elif [[ $inp == "hi" ]]
    then 
    echo "Hello, `whoami`"
  elif [[ $inp == "exit" ]]
    then
    break
  else
    $inp      
  fi



done
