outp=0
arr=()
res=0

function first() {
    for i in $@
    do
        echo "Args: $i"
    done
    
    for i in $@
    do
        outp=$(( $i * $i ))
        arr+=($outp)
        echo "Multipl: $outp"
    done
}

function second() {
    first 12 1 3
    
    for i in ${!arr[@]}
    do
       sum=$(( ${arr[$i]} + 1 ))
       echo "Result: $sum"
    done
}

second
