#!/bin/bash

read -p 'Enter filename: ' usrFile

head -c 4KB /dev/urandom > $usrFile
touch helperfile.txt
# Working but with deleting 3 last characters (K KB)
val=$(ls -l $usrFile | awk '{print $5}' | sed 's/...$//')

until [ $val -gt 1024 ]
do
  val=$(ls -l $usrFile | awk '{print $5}' | sed 's/...$//')
  echo "Filesize(KB): $val"
  cat $usrFile > helperfile.txt
  cat helperfile.txt >> $usrFile
done



