#!/bin/bash

read -p "Input a filename: " usrFile
touch $usrFile

echo "An old silent pond...
A frog jumps into the pond,
splash! Silence again.

Autumn moonlight-
a worm digs silently
into the chestnut.

In the twilight rain
these brilliant-hued hibiscus -
A lovely sunset." | tee $usrFile 

./script.sh > /dev/null 2>&1

