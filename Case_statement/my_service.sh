#!/bin/bash

case $1 in
  start)
    sleep 9999
    echo "Service started"
    ;;
  stop)
    kill $(ps aux | grep "/bin/bash my_service.sh" | awk '{printt $2}')
    echo "Service is stopped"
    ;;
  restart)
    kill $(ps aux | grep "/bin/bash my_service.sh" | awk '{printt $2}')
    echo "Service is stopped"
    sleep 9999
    echo "Service is started"
    ;;
  *)
    echo "Usage: my_service.sh [start|stop|retsart|help]"
    ;;
esac
