#!/bin/bash
cnt=1
arr=()
sum=0
iter=1 

# Add arguments to array
for i in $@
do
    arr+=($i)
done

# Get the array length
arrLeng=${#arr[@]}

# Loop for output all arguments of array
for q in "${arr[@]}"
do
    echo "Arg$cnt: $q"
    cnt=$(expr $cnt + 1)
done

# Output the array length
echo "#########################"
echo "Arrlength: $arrLeng"
echo "#########################"

# Get index of each array element
# Sum counts with getting the current element and index+1(next element)
# If my counter == length of arr then sum == curr element + first element of array 
for item in ${!arr[@]}
do
           
    if [[ $iter -eq $arrLeng ]]
    then
      sum=$(expr ${arr[$item]} + ${arr[0]})
      echo "$sum"
    else
      sum=$(expr ${arr[$item]} + ${arr[$( expr $item + 1)]})
      echo -n "$sum "
    fi

    iter=$(expr $iter + 1) 
done 

